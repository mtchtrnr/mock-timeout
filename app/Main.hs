{-# LANGUAGE NumericUnderscores #-}

module Main where

import Lib (ExpirableOperation (..), runExpirable, systemTaskTimer)

import Control.Concurrent     (threadDelay)
import Data.Time.Clock ( secondsToDiffTime)

main :: IO ()
main = runExpirable systemTaskTimer exampleOp

exampleOp :: ExpirableOperation () ()
exampleOp
  = ExpOp
  { longTask = threadDelay 5_000_000  -- in microseconds
  , timeoutDuration = secondsToDiffTime 3
  , handleExpired = putStrLn "Uh-oh, too late now"
  , handleUnexpired = putStrLn "Way to meet the deadline, kid!"
  }
