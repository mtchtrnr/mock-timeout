module Lib
    ( ExpirableOperation (..),
    runExpirable,
    systemTaskTimer
    ) where

import Control.Concurrent     (threadDelay)
import Data.Time.Clock.System (getSystemTime, systemToTAITime, SystemTime)
import Data.Time.Clock.TAI    (diffAbsoluteTime)
import Data.Time.Clock        (DiffTime, secondsToDiffTime)

data ExpirableOperation a b
  = ExpOp
  { longTask        :: IO a
  , timeoutDuration :: DiffTime
  , handleExpired   :: IO b
  , handleUnexpired :: IO b
  }

systemTaskTimer :: IO a -> IO DiffTime
systemTaskTimer task =
  do
    startedAt <- fmap systemToTAITime getSystemTime
    task
    endedAt <- fmap systemToTAITime getSystemTime

    return (diffAbsoluteTime endedAt startedAt)

runExpirable :: (IO a -> IO DiffTime) -> ExpirableOperation a b -> IO b
runExpirable timer operation =
  do
    elapsed <- timer (longTask operation)

    if elapsed > timeoutDuration operation
      then handleExpired operation
      else handleUnexpired operation
