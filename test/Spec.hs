{-# LANGUAGE NumericUnderscores #-}

import Lib (ExpirableOperation (..), runExpirable)

import Data.Time.Clock        (DiffTime, secondsToDiffTime)
import Test.HUnit (assertBool)
import Test.QuickCheck (quickCheck)
import Test.QuickCheck.IO (propertyIO)

main = quickCheck proptest_runExpirable

mockTaskTimer :: IO Integer -> IO DiffTime
mockTaskTimer task = fmap secondsToDiffTime task

mockTask :: Integer -> IO Integer
mockTask a = return a

proptest_runExpirable wait timeout = propertyIO (runExpirable mockTaskTimer op)
  where
  op = if wait > timeout
   then shouldExpire wait timeout
   else shouldNotExpire wait timeout

shouldExpire wait timeout
  = ExpOp
  { longTask = mockTask wait
  , timeoutDuration = secondsToDiffTime timeout
  , handleExpired = assertBool message True
  , handleUnexpired = assertBool message False
  } where
  message = "Wait " ++ (show wait) ++ ", Timeout " ++ (show timeout) ++ ", should expire"

shouldNotExpire wait timeout
  = ExpOp
  { longTask = mockTask wait
  , timeoutDuration = secondsToDiffTime timeout
  , handleExpired = assertBool message False
  , handleUnexpired = assertBool message True
  } where
  message = "Wait " ++ (show wait) ++ ", Timeout " ++ (show timeout) ++ ", should not expire"



